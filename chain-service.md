---
description: The Chain Service makes the statechain consumable by clients.
---

# Chain Service

### Overview

The Chain Service is a service maintained that scans the statechain for events and makes it consumable to clients. The Chain Service can be replayed from the start of the Statechain at any time. 

The Chain Service uses an InfluxDB in order to populate entries and allow easy queries across the time-domain. 

The data maintained by the Chain Service is covered in the API Documenation. 



