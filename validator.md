# Validator

The Validator in the **Service Node** processes blocks using the CosmosSDK application-blockchain. The Validators exist in three stages of their life-cycle:

1. Genesis
2. Active
3. Standby

On **Genesis** the Validators apply to be whitelisted via either Proof-of-Authority or later, Proof-of-Stake. When Validators are **Active**, they are proposing, voting and committing blocks to the statechain in accordance with Application Logic. When Validators are on **Standby**, they are syncing the chain, but are not part of consensus. 

### Genesis & Whitelisting

When a Validator starts, it generates a statechain validator key and applies to be whitelisted by sending its key to the existing admins. The existing admins will whitelist the validator key via an ADMIN transaction that requires 67% consensus. 

The statechain validator key has a `bepv` prefix. 

After a duty cycle, the Validator will be included in consensus. 

### Application Logic

The BEPSwap application logic does the following things:

1. Comes to agreement on observed events
2. Applies logic to finalised events
3. Outputs a transaction array

#### Observation Consensus

Observation Consensus is the logic that is required to count votes on identical observed events, which are Binance Chain blocks containing transactions. A `block-tx` object is passed into the statechain, uniquely identified by its blockheight, and if unique, is created. Other observers also report at the same height. 

If the reported `block-tx` is identical, then observations count as votes. If the reported `block-tx` is not identical, then a new `block-tx` is created. Up to `n` different `block-tx` can be created with `n` Observers. 

As votes are counted, then the `block-tx` with 67% of the votes is finalised, and all competing `block-tx` are removed and the `Observers` that made those observations are bumped to the bottom of the Duty-Cycle list. 

#### Finalised Event Logic

When events are finalised, they can be any of the permitted transactions and each transaction results in a State Change and for some, an Output Tx. 

<table>
  <thead>
    <tr>
      <th style="text-align:left">Event</th>
      <th style="text-align:left">STATE CHANGE</th>
      <th style="text-align:left">OUTPUT TX</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">CREATE</td>
      <td style="text-align:left">Creates a new pool, populates into various arrays. Adds staker and balances.</td>
      <td
      style="text-align:left">Only if refund is needed.</td>
    </tr>
    <tr>
      <td style="text-align:left">STAKE</td>
      <td style="text-align:left">Adds staker and balances. Updates pool units.</td>
      <td style="text-align:left">Only if refund is needed.</td>
    </tr>
    <tr>
      <td style="text-align:left">WITHDRAW</td>
      <td style="text-align:left">Removes staker, decrements stake units, pool units, balances.</td>
      <td
      style="text-align:left">Payout transaction.</td>
    </tr>
    <tr>
      <td style="text-align:left">SWAP</td>
      <td style="text-align:left">Increments/Decrements pool balances.</td>
      <td style="text-align:left">Refund or Payout transaction.</td>
    </tr>
    <tr>
      <td style="text-align:left">GAS</td>
      <td style="text-align:left">Adds to the BNB gas balance.</td>
      <td style="text-align:left">None</td>
    </tr>
    <tr>
      <td style="text-align:left">ADD</td>
      <td style="text-align:left">Adds to the pool balances.</td>
      <td style="text-align:left">None</td>
    </tr>
    <tr>
      <td style="text-align:left">ADMIN</td>
      <td style="text-align:left">Changes global parameters.</td>
      <td style="text-align:left">None</td>
    </tr>
    <tr>
      <td style="text-align:left">OUTBOUND</td>
      <td style="text-align:left">
        <p>Finalises <code>txArray</code> 
        </p>
        <p>Generated automatically as part of an observed outgoing transaction.</p>
      </td>
      <td style="text-align:left">None</td>
    </tr>
  </tbody>
</table>#### Transaction Output

Transaction objects generated as part of the state change are compiled into a single `txOut` array that is first created in the statechain `beginBlock` method. 

```text
{
“TXArrayID” : STATECHAIN-BLOCKHEIGHT,
 "hash" : "",
 "txArray" : [{ “to” : "USERAaddr"
	            “amount”: "200000000"
		        “symbol”: "BNB"},
              { “to” : "USERBaddr"
	            “amount”: "200000000"
		       “symbol”: "RUNE-A1F"}]
}

```

The `txOut` array contains the statechain-blockheight to identify it, the final Binance Chain hash \(which will be empty\) as well as the `txArray` that has the to, amount and token. 

### Standby

Every duty cycle the validators remove one from consensus and add one from the standby set. The validator that is chosen to be removed is the one at the top of the duty cycle list, which is influenced by a metric which tracks their position. 

Every block that a Validator achieves 100% uptime with no errors, then their score does not change. For every duty cycle, the top Validator is chosen and they are removed \(highest score\). A new validator is chosen from the Standby list and their starting score is 0, and all other validators are incremented by 1.

Certain misbehavioiurs also accrue penalties, which accelerates their score and pushes them to the top of the list so they they are removed first. Validators that have the same score are chosen by mathematical ordering of their public key. 

| Type | Notes | Penalty |
| :--- | :--- | :--- |
| Incorrect Observed Event | An Observer incorrectly observes an event.  | +n |
| Missed Event | An Observer misses an event. | +0.5n |
| Skipped block  | A Validator fails to propose a block | +0.5n |
| Failed To Sign | A Signer fails to sign a transactions | +n |

{% hint style="info" %}
`n` here refers to the number of Active Validators. By penalising them with an amount equal to `n` then they are pushed to the top of the list and will the first to exit. 
{% endhint %}

Whilst on Standby, the Validator can cycle and update their machine, or use their Admin key to update their Observer, Validator or Signer keys. 

When it is time to enter consensus again, they join the TSS p2p protocol and participate in key-gen ceremony to create a new pool address. Once finished, their signer must complete a **SIGNER** transaction to change the pool address. 100% participation is required and if they fail to be present, then their key-set is removed from the statechain and the next standby validator is chosen to join. 

This ensures that the duty cycle rotates so that only active, online validators are included back in consensus. 

