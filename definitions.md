---
description: >-
  This page describes all the definitions and glossary of terms used in the
  BEPSwap architecture.
---

# Definitions

### Acronyms

| Name | Description | Example |
| :--- | :--- | :--- |
| CLP | Continuous Liquidity Pool that uses the XYK liquidity protocol. | `RUNE:BNB` |
| XYK | "The  $$x * y = k$$ bonded token liquidity protocol suggested by [Vitalik in 2016 on Reddit](https://www.reddit.com/r/ethereum/comments/55m04x/lets_run_onchain_decentralized_exchanges_the_way/), implemented by [UniSwap](https://uniswap.io/) and BEPSwap. |  |
| BFT | Byzantine Fault Tolerance which can tolerate up to `n/3-1` malicious nodes, where `n` is the total number of nodes in consensus. | `8 of 11` |
| TSS | Threshold Signature Scheme, using Genarro-Goldfeder 2018. [https://eprint.iacr.org/2019/114.pdf](https://eprint.iacr.org/2019/114.pdf) | `3 of 4` |
| MPC | Multi-Party Computation used in TSS to calculate shared secrets, generate public keys and signatures. |  |
| P2P | Peer-To-Peer protocol to populate messages in MPC. |  |
| ECDSA | Eliptical Curve Digital Signature Algorithm signature scheme. |  |
| BECH32 | BECH32 encoded address, originally derived by [Pieter Wuillie and Greg Maxwell](https://github.com/bitcoin/bips/blob/master/bip-0173.mediawiki) for Bitcoin. Includes a prefix and a check-sum to prevent address errors.  | `tbnb1mh3w2kxmdmnvctt7t5nu7hhz9jnp422edqdw2d` |

### 

### 

### Glossary

<table>
  <thead>
    <tr>
      <th style="text-align:left">Name</th>
      <th style="text-align:left">Description</th>
      <th style="text-align:left">Example</th>
    </tr>
  </thead>
  <tbody>
    <tr>
      <td style="text-align:left">Asset/Token</td>
      <td style="text-align:left">An object that describes a unique digital asset. Here returned from:
        <a
        href="https://dex.binance.org/api/v1/tokens?limit=200">https://dex.binance.org/api/v1/tokens?limit=200</a>
      </td>
      <td style="text-align:left"><code>{&quot;mintable&quot;:true, &quot;name&quot;:&quot;Loki&quot;, &quot;original_symbol&quot;:&quot;LOKI&quot;, &quot;owner&quot;: &quot;bnb1j5sft8wp7tktjwauy30x79f3tqa53fycmgxxs0&quot;, &quot;symbol&quot;:&quot;LOKI-6A9&quot;, &quot;total_supply&quot;:&quot;3000000.00000000&quot;}</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Symbol</td>
      <td style="text-align:left">
        <p>The Identifier for an Asset, given by Binance Chain, in form <code>TICKER789-XXX</code> (max
          9 characters, with <code>-XXX</code> given by Binance Chain automatically
          on-chain.)</p>
        <p><a href="https://docs.binance.org/faq.html#how-can-i-issue-an-asset">https://docs.binance.org/faq.html#how-can-i-issue-an-asset</a>
        </p>
      </td>
      <td style="text-align:left"><code>LOKI-6A9</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Ticker/Original Symbol</td>
      <td style="text-align:left">The exchange ticker for the asset. It can also be derived from symbol
        by stripping the <code>-XXX</code>. Tickers are used to also identify assets
        in the exchange interface, but may be duplicative.</td>
      <td style="text-align:left"><code>LOKI</code>
      </td>
    </tr>
    <tr>
      <td style="text-align:left">Service Node</td>
      <td style="text-align:left">Each Service Node in BEPSwap has a co-located Observer that reports on
        events, a Validator that processes state changes, and a Signer that signs
        outgoing transactions.</td>
      <td style="text-align:left">Observer-Validator-Signer</td>
    </tr>
    <tr>
      <td style="text-align:left">Observer</td>
      <td style="text-align:left">An Observer reports to the Statechain of events seen from an external
        source.</td>
      <td style="text-align:left"></td>
    </tr>
    <tr>
      <td style="text-align:left">Validator</td>
      <td style="text-align:left">A Validator comes to consensus on state change in the statechain, using
        Tendermint.</td>
      <td style="text-align:left"></td>
    </tr>
    <tr>
      <td style="text-align:left">Signer</td>
      <td style="text-align:left">A Signer queries for transactions to sign using the TSS service, and send
        to an external blockchain.</td>
      <td style="text-align:left"></td>
    </tr>
    <tr>
      <td style="text-align:left">Statechain</td>
      <td style="text-align:left">The statechain is a CosmosSDK powered replicated state machine that does
        not peg assets.</td>
      <td style="text-align:left"></td>
    </tr>
  </tbody>
</table>

