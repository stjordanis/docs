---
description: >-
  Describes how the Observer works, start-up, block-scanning and observation
  consensus.
---

# Observer

The Observer in the **Service Node** is simply a block-scanner that scans Binance Chain and monitors for transactions that involve the pool address. When a transaction is observed, the Observer reports it to the Statechain. 

### Start-up & Whitelisting

When the Observer first starts, it generates a statechain transaction address and requests to be whitelisted. The statechain address is a BECH32 address with `bep` prefix:

```text
bep1uvghr0lzn5d8ecytljr9pvy8uss2ak3dawgqqm
```

When it is whitelisted, it is added to the list of Observers that can make transactions into the statechain. In the process of whitelisting, it is sent a number of `bep`gas tokens in order to pay for gas on the statechain. 

> Gas is actually not required, but to prevent forking from the CosmosSDK the concept of gas is retained.

The observer then queries the statechain for the latest successfully completed Binance Chain block in order to know where to start. 

### Block Scanning

The Observer connects to either a Binance Chain full node, or a remote node \([peers are here](https://dex.binance.org/api/v1/peers)\).

Each block is scanned for transactions involving the pool address, using the Binance Chain [block scan API](https://docs.binance.org/api-reference/dex-api/paths.html#apiv1transactions-in-blockblockheight), and all blocks are reported back to the statechain.

> Binance Chain blocks are 300ms, so if statechain blocks are 3 seconds then there should be 10 blocks observed in each statechain block. Each block will generate 0, 1 or 2+ transactions. \(most likely 0, occasionally 1\).

Here are three Binance Chain blocks, with single, multiple and no transactions observed. These transaction arrays will sit in state for the Observer, until it receives a commitment hash from the statechain, when it will log them as finalised, since its job is done.

Each block is reported on for 100% coverage, and each transaction includes block height, and transaction count.

**Single Transaction**

```text
{"blockHeight": 121,
"txArray":     [{ "tx" : "XXXX",   
              "amount": "XX",
               "memo" : "XXXX",
              "token": "SYMBOL-XXX"}]
}
```

The Binance Chain blockheight and the transaction objects are collected and reported on. The transaction object is simply its hash, the amount, token transferred and the transaction `memo`.

#### Two or more transactions

```text
{"blockHeight": 121,
"txArray":     [{ "tx" : XXXX,     
              "Amount": "XX",
               "MEMO" : "XXXX",
              "Token": "SYM"}, 
              { "tx" : XXXX,   
              "Amount": "XX",
               "MEMO" : "XXXX",
              "Token": "SYM"}]
}
```

Multiple transactions in a block are tracked in the transaction array. 

#### Empty Block

```text
{"blockHeight": 123,
"txArray":     []
} 
```

Empty blocks are reported on regardless, simply to get 100% coverage and ensure the statechain can be re-started successfully if it is every halted for some reason. 

### Observation Consensus

Each Observer \(which is always co-located with a Service Node\) submits every Binance Chain block they see as block-tx. When entered into the statechain, the block-tx is tagged as `pending`

The block proposer for the round compiles votes for each block-tx from each whitelisted Observer, and as long as the contents of each block reported is identical, then each block-tx in the statechain becomes `finalised`

When it is `finalised` each transaction in that block-tx becomes fully-ordered allowing BEPSwap logic to be applied to the contents of the transaction. 

{% hint style="info" %}
Assuming honest number of Service Nodes of at least 2 / 3, and fully-functioning Observers, then each Binance Chain block will be reported on with enough votes to be finalised at each Statechain block. 

If a block-tx does not attract 2 / 3 votes then transactions will stop being reported on. This does not cause the BEPSwap chain to halt though, it will continue with empty blocks. 
{% endhint %}

### Observer Halts

In the unlikely event the Observer blockscan progress halts then it will be because it cannot get 67% majority agreement on a block that has been reported on. This may be because of a disconnection with a Binance Chain node. 

The last finalised block can be easily queried from the statechain. The Observers are then made to rescan at that height until agreement is attained and progress can continue. 

Since blocks are progressed in the order that they appear in Binance Chain, the Observer can safely halt, rescan and pick up progress.

