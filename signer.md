---
description: 'Querying, signing and sending outgoing transactions.'
---

# Signer

The Signer in the **Service Node** queries for unsigned transaction arrays from the Statechain and then prepares them for signing by converting them into Binance Chain transaction payloads. Once prepared, it is sent to the TSS Module to sign using MPC and broadcast to Binance Chain.

The signing process is based on [Generra-Goldfeder 2018](https://eprint.iacr.org/2019/114.pdf). 

Only one of the signers will receive a commitment hash back from Binance Chain, the rest will receive a double spend error which is expected. The Signers will then converge on the next unsigned transaction array and repeat. 

### Querying

The first step is to query the statechain for the latest unsigned blockheight. This marks the latest unsigned statechain transaction array since they are ID'd by statechain blockheight. 

`{{statechain}}/lastblock`

```text
{
 "bnbIn" : "12345",
 "bepOut" : "1234",    //latest signed array
 "stateChain" : 1236   //statechain blockheight
}
```

Normally there should only be 1-2 blocks difference between `bepOut` and `statechain` which details how many unsigned arrays there are, but if the Signer is coming online after some time, then there could be many. 

From there, the blockheight `bepOut` is incremented and then queried for the transaction array:

`{{statechain}}/txoutarray/1235`

```text
{“TXArrayID” : 1235,
 "hash" : "",
 "txArray" : [{ “to” : "USERAaddr"
	         “Amount”: "200000000"
		     “Token”: "BNB"},
               { “to” : "USERBaddr"
	          “Amount”: "200000000"
		      “Token”: "RUNE-A1F"}]
}
```

This transaction array is then prepared into a valid Binance Chain transaction payload, which is in accordance with the [encoding](https://docs.binance.org/encoding.html) requirements of Binance Chain:

```text
{
   "sequence" : "64",
   "account_number" : "12",
   "data" : null,
   "chain_id" : "chain-bnb",
   "memo" : "OUTBOUND:1235",
   "msgs" : [
      {
         "inputs" : [
            {
               "coins" : [
                  {
                     "denom" : "BNB",
                     "amount" : "200000000"
                  }, 
                  {
                     "denom" : "RUNE-A1F",
                     "amount" : "200000000"
                  }
               ],
               "address" : "POOLADDRESS"
            }
         ],
         "outputs" : [
            {
               "address" : "USERAaddr",
               "coins" : [
                  {
                     "denom" : "BNB",
                     "amount" : "200000000"
                  }
               ]
            }, 
            {
               "address" : "USERBaddr",
               "coins" : [
                  {
                     "denom" : "RUNE-A1F",
                     "amount" : "200000000"
                  }
               ]
            }
         ]
      }
   ],
   "source" : "1"
}
```

The `TXArrayID` \(which is just the statechain blockheight that it occurs in\) is packaged into the `MEMO` field in order to identify it later. 

This is then cached and repeated for all arrays that are unsigned in order to parallel-sign them and work through the backlog. The cache is regularly refreshed to remove transactions that have found to be signed in the statechain, and add more that are unsigned. 

{% hint style="info" %}
Since the statechain is replicated across all Service Nodes, then all Signers participating in the network will have full state-replication and should all have identical transaction payloads, sourced from their own state storage. 

This minimises attack vectors, since Signers are only signing transactions they have already agreed to as part of statechain block validation. 
{% endhint %}

### Signing

Once the signers have the transaction payload they then subscribe to p2p channel that is uniquely identified by the `TXArrayID` and begin a multi-round signing process to correctly sign the transaction. Ie, to sign transaction `1235` the channel ID is also `1235`. If 10 different transaction payloads are being signed, `1235 : 1245` then the signers will subscribe to 10 different channels. 

If the signing process fails, then it is because of the following reasons:

1. Not enough signers present to meet the threshold requirements
2. Mismatched transaction payload 

In either case, the signers who are being honest continue to stay on and engage in the process until either the process is complete or in refreshing their cache they find it has been signed already. 

{% hint style="info" %}
For the case that there are many unsigned transactions, Signers can subscribe to as many channels as their resources allow, starting from the lowest transaction ID. In this way a backlog of transactions can be cleared out as fast as there are a minimum number of signers needed to sign a transaction present in a channel. 
{% endhint %}

{% hint style="info" %}
Outgoing transaction arrays do not need to be sent in the correct order, since the Statechain has already provisioned their funds and incremented balances correctly.
{% endhint %}

### Sending

Once the transaction is signed from a successful signing ceremony, all signers broadcast it to BinanceChain. Only signers that were part of that channel will have the signed transaction payload. Signers will get either of the two messages:

`//Successful  
Committed at block 33600477 (tx hash: 4FB8096A93D545612A3B5DCE520622608C299C7742103A6BE34C444829BD83A5)`

`//Duplicate  
ERROR: broadcast_tx_commit: Response error: RPC error -32603 - Internal error: Error on broadcastTxCommit: Tx already exists in cache`

If either of the two messages are received, then the signer will know the transaction has been sent, unsubscribe from that channel ID and remove the transaction from local cache. 

If a Signer has a particular transaction in their cache, but was not part of that channel and joins after it was successfully signed, then they will not know of this until it is observed by their Observer, or they see it in their Statechain. 





